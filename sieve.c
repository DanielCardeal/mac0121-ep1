#include <stdio.h>
#include <stdlib.h>

int is_prime(int n);

int main(int argc, char *argv[]) {
    int n;
    n = atoi(argv[1]);
    printf("%d\n", is_prime(n));
    return 0;
}

int is_prime(int n) {
    int i;
    for ( i = 2; i < n; i++ )
        if ( n % i == 0 )
           return 0;
    return 1;
}
