global _start ; "mostra" _start no escopo global
extern main   ; avisa que main é externa a esse arquivo

section .text
_start:
	; Zera o stack frame
	xor ebp, ebp

	; Salva o argc no registrador esi
	pop esi

	; Salva o argv no registrador ecx sem mover o stack pointer
	mov ecx, esp

	; Limpa os 4 últimos bits do stack pointer para que ele volte
    ; a apontar para o mesmo endereço da chamada da função
	and esp, 0xfffffff0

	; Empilha o argv
	push ecx

	; Empilha o argc
	push esi

	; Chama a main
	call main

	; Recebe o número da chamada de sistema (sys_exit)
	mov	eax, 1

	; Instrução de interrupção
	int	0x80
