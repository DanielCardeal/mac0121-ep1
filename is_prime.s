global is_prime ; "mostra" is_prime no escopo global
section .text
is_prime:
    ; Salva a posição antiga da base da stack para recuperar no final da
    ; função
    push ebp
    mov  ebp, esp
    
    ; -- Inicialização do contador i
    mov ecx, 0x2      ; i = 2
.loop:
    ; Recebe o primeiro argumento no registrador eax
    mov eax, [ebp+8]

    ; Testa se chegou ao final do loop
    cmp ecx, eax
    jge .e_primo 

    ; Limpa o registrador onde será salvo o resto da divisão
    xor edx, edx

    ; n % i -> edx
    div ecx

    ; Se o resto for 0, n não é primo
    cmp edx, 0x0
    je .nao_e_primo 

    ; Caso contrário, apenas continua o loop
    inc ecx ; i++
    jmp .loop
.e_primo:
    ; Coloca 1 como retorno da função (n é primo)
    mov eax, 0x1
    jmp .volta
.nao_e_primo:
    ; Coloca 0 como retorno da função (n não é primo)
    mov eax, 0x0
    jmp .volta
.volta:
    ; Recupera o esp
    mov esp, ebp
    ; Recupera e retorna à posição antiga do base pointer
    pop ebp
    ret 
