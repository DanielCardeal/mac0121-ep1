#include <stdio.h>
#include <stdlib.h>

extern int is_prime(int n);

int main(int argc, char *argv[]) {
    int n;
    n = atoi(argv[1]);
    printf("%d\n", is_prime(n)); 
    return 0;
}
