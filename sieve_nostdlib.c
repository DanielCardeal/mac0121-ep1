extern int is_prime(int n);
extern void print_asm(int length, char *string);

int string_to_int(char *string) {
    int res = 0, i;
    for ( i = 0; string[i] != '\0'; i++ ) {
        res = res * 10 + string[i] - '0';
    }
    return res;
}

int main(int argc, char *argv[]) {
    int n;
    n = string_to_int(argv[1]);
    if ( is_prime(n) )
        print_asm(2, "1\n");
    else
        print_asm(2, "0\n");
    return 0;
}
