CC=gcc
CFLAGS=-Wall

all: sieve sieve_asm sieve_nostdlib

sieve: sieve.c
	$(CC) $(CFLAGS) -o $@ $^

sieve_asm: sieve_asm.c is_prime.o
	$(CC) $(CFLAGS) $^ -m32  -o $@

sieve_nostdlib: sieve_nostdlib.c _start.o print.o is_prime.o
	$(CC) $(CFLAGS) -m32 -nostdlib $^ -o $@

%.o: %.s
	nasm -f elf32 $< -o $@

clean:
	rm sieve_nostdlib sieve_asm
	rm sieve _start.o print.o is_prime.o
