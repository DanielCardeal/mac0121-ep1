global print_asm ; "mostra" print_asm no escopo global

section .text
print_asm:
	; Salva a posição antiga da base da stack para recuperar no
    ; final da função
    push ebp
    mov  ebp, esp

    ; Recebe o primeiro argumento, que contém o tamanho da string
    mov edx, [ebp+8]

    ; Recebe o segundo argumento, que contém a string
    mov ecx, [ebp+12]

    ; Recebe o número da saída (stdout)
    mov ebx, 1

    ; Recebe o número da chamada (sys_write)
    mov eax, 4

    ; Chama o kernel
    int 0x80

    ; Recupera o esp
    mov esp, ebp

    ; Recupera e retorna à posição antiga do base pointer
    pop ebp
    ret
